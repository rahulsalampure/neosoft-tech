package com.workproject.repository;

import com.workproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByUserName(String userName);

    List<User> findAllBySurname(String surname);

    List<User> findAllByPinCode(Integer pinCode);

    List<User> findAllByOrderByJoiningDateAsc();

    List<User> findAllByOrderByBirthDateAsc();

    @Transactional
    @Modifying
    @Query(value = "UPDATE User uuser SET uuser.disabled = ?1 WHERE uuser.id= ?2")
    void disableUserById(boolean flag,Long id);
}
