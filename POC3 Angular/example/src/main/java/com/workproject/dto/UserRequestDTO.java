package com.workproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRequestDTO {

    private String userName;

    private String surname;

    private String contactNo;

    private Date birthDate;

    private Date joiningDate;

    private String gender;

    private String email;

    private String address;

    private Integer pinCode;
}
