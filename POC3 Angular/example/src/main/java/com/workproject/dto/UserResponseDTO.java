package com.workproject.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponseDTO {

    private Long id;

    private String userName;

    private String surname;

    private String contactNo;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date joiningDate;

    private String gender;

    private String email;

    private String address;

    private Integer pinCode;
}
