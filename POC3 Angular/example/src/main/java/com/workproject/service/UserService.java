package com.workproject.service;

import com.workproject.dto.UserRequestDTO;
import com.workproject.model.User;
import com.workproject.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
@AllArgsConstructor
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class.getName());

    @Autowired
    private UserRepository userRepository;

    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    public User save(UserRequestDTO userRequestDTO) {
        try {
            return userRepository.save(copy(userRequestDTO));
        } catch (Exception exception) {
            logger.error("Error Occurred!!!: {}", exception);
        }
        return new User();
    }

    public List<User> getAllByUsername(final String userName) {
        return userRepository.findAllByUserName(userName);
    }

    public List<User> getAllBySurname(final String surname) {
        return userRepository.findAllBySurname(surname);
    }

    public List<User> getAllByPinCode(final Integer pinCode) {
        return userRepository.findAllByPinCode(pinCode);
    }

    public List<User> getAllByDobAsc() {
        return userRepository.findAllByOrderByBirthDateAsc();
    }

    public List<User> getAllByJoiningAsc() {
        return userRepository.findAllByOrderByJoiningDateAsc();
    }

    public User updateUser(UserRequestDTO userRequestDTO, Long id) {
        try {
            if ((!userRepository.existsById(id)) || userRepository.findById(id).get().isDisabled()) {
                return new User();
            }
            User user = copy(userRequestDTO);
            user.setId(id);
            return userRepository.save(user);
        } catch (Exception exception) {
            logger.error("Error Occurred!!!: {}", exception);
        }
        return new User();
    }

    public ResponseEntity disable(Long id) {
        if (!userRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userRepository.disableUserById(true, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity delete(Long id) {
        if(!userRepository.existsById(id)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public static User copy(final UserRequestDTO userRequestDTO) {
        final User user = new User();
        user.setUserName(userRequestDTO.getUserName());
        user.setSurname(userRequestDTO.getSurname());
        user.setContactNo(userRequestDTO.getContactNo());
        user.setBirthDate(userRequestDTO.getBirthDate());
        user.setJoiningDate(userRequestDTO.getJoiningDate());
        user.setGender(userRequestDTO.getGender());
        user.setEmail(userRequestDTO.getEmail());
        user.setAddress(userRequestDTO.getAddress());
        user.setPinCode(userRequestDTO.getPinCode());

        return user;
    }
}
