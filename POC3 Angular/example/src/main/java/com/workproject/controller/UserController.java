package com.workproject.controller;

import com.workproject.dto.UserRequestDTO;
import com.workproject.dto.UserResponseDTO;
import com.workproject.model.User;
import com.workproject.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserResponseDTO>> getAllUsers() {
        return reduce(userService.findAllUser());
    }

    @PostMapping
    public ResponseEntity<UserResponseDTO> registerUser(@RequestBody UserRequestDTO userRequestDTO) {
        return new ResponseEntity<>(transform(userService.save(userRequestDTO)), HttpStatus.CREATED);
    }

    @GetMapping("/username/{userName}")
    public ResponseEntity<List<UserResponseDTO>> getUsersByName(@PathVariable String userName) {
        return reduce(userService.getAllByUsername(userName));
    }

    @GetMapping("/surname/{surname}")
    public ResponseEntity<List<UserResponseDTO>> getUsersBySurname(@PathVariable String surname) {
        return reduce(userService.getAllBySurname(surname));
    }

    @GetMapping("/pincode/{pinCode}")
    public ResponseEntity<List<UserResponseDTO>> getUsersByPinCode(@PathVariable Integer pinCode) {
        return reduce(userService.getAllByPinCode(pinCode));
    }

    @GetMapping("/list-by-dob")
    public ResponseEntity<List<UserResponseDTO>> getUsersByDobAscending() {
        return reduce(userService.getAllByDobAsc());
    }

    @GetMapping("/list-by-joining-date")
    public ResponseEntity<List<UserResponseDTO>> getUsersByJoinAscending() {
        return reduce(userService.getAllByJoiningAsc());
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> updateUser(@RequestBody UserRequestDTO userRequestDTO,@PathVariable Long id) {
        User user = userService.updateUser(userRequestDTO, id);
        if(user.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(transform(user), HttpStatus.ACCEPTED);
    }

    @PutMapping("/disable/{id}")
    public ResponseEntity disableUser(@PathVariable("id") Long id){
        return userService.disable(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        return userService.delete(id);
    }

    public static ResponseEntity reduce(List<User> users) {
        List<UserResponseDTO> userResponseDTO = users.stream().map(UserController::transform).collect(toList());
        return userResponseDTO.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND):
                new ResponseEntity<>(userResponseDTO, HttpStatus.OK);
    }

    public static UserResponseDTO transform(final User user) {

        final UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setUserName(user.getUserName());
        userResponseDTO.setSurname(user.getSurname());
        userResponseDTO.setContactNo(user.getContactNo());
        userResponseDTO.setBirthDate(user.getBirthDate());
        userResponseDTO.setJoiningDate(user.getJoiningDate());
        userResponseDTO.setGender(user.getGender());
        userResponseDTO.setEmail(user.getEmail());
        userResponseDTO.setAddress(user.getAddress());
        userResponseDTO.setPinCode(user.getPinCode());

        return userResponseDTO;
    }
}
