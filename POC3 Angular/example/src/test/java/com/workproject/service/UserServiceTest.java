package com.workproject.service;

import com.workproject.dto.UserRequestDTO;
import com.workproject.model.User;
import com.workproject.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Captor
    private ArgumentCaptor<User> postArgumentCaptor;

    @Test
    @DisplayName("Should Save Users")
    public void shouldSaveUsers() {
        UserRequestDTO userRequestDTO = new UserRequestDTO("UserName1", "Surname1", "9999911111", new Date(1980 - 01 - 01),
                new Date(2017 - 01 - 01), "male", "fds@xyz.com", "Wall Street NY", 220001);

        userService.save(userRequestDTO);

        Mockito.verify(userRepository, times(1)).save(postArgumentCaptor.capture());

        Assertions.assertThat(postArgumentCaptor.getValue().getUserName()).isEqualTo("UserName1");
        Assertions.assertThat(postArgumentCaptor.getValue().getSurname()).isEqualTo("Surname1");
    }


    @Test
    @DisplayName("Should Get All Users By Username")
    public void shouldGetAllByUsername() {
        List<User> users = new ArrayList();
        users.add( new User(1L, "UserName1", "Surname1", "9999911111", new Date(1981-01-01),
                new Date(2018-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(2L, "UserName1", "Surname2", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(3L, "UserName1", "Surname3", "9999922222", new Date(1983-01-01),
                new Date(2015-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));

        given(userRepository.findAllByUserName("Username1")).willReturn(users);

        List<User> expected = userService.getAllByUsername("Username1");

        assertEquals(expected, users);
    }

    @Test
    @DisplayName("Should Get All Users By Surname")
    public void shouldGetAllBySurname() {
        List<User> users = new ArrayList();
        users.add( new User(1L, "UserName1", "Surname1", "9999911111", new Date(1981-01-01),
                new Date(2018-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(2L, "UserName1", "Surname1", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(3L, "UserName1", "Surname1", "9999922222", new Date(1983-01-01),
                new Date(2015-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));

        given(userRepository.findAllBySurname("Surname1")).willReturn(users);

        List<User> expected = userService.getAllBySurname("Surname1");

        assertEquals(expected, users);
    }

    @Test
    @DisplayName("Should Get All Users By PinCode")
    public void shouldGetAllByPinCode() {
        List<User> users = new ArrayList();
        users.add( new User(1L, "UserName1", "Surname1", "9999911111", new Date(1981-01-01),
                new Date(2018-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(2L, "UserName2", "Surname2", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(3L, "UserName3", "Surname3", "9999922222", new Date(1983-01-01),
                new Date(2015-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));

        given(userRepository.findAllByPinCode(220001)).willReturn(users);

        List<User> expected = userService.getAllByPinCode(220001);

        assertEquals(expected, users);
    }

    @Test
    @DisplayName("Should Sort All Users By DOB Ascending")
    public void shouldGetAllUsersByDobAsc() {
        List<User> users = new ArrayList();
        users.add( new User(3L, "UserName1", "Surname1", "9999911111", new Date(1983-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(2L, "UserName2", "Surname2", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(1L, "UserName3", "Surname3", "9999933333", new Date(1981-01-01),
                new Date(2018-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));

        given(userRepository.findAllByOrderByBirthDateAsc()).willReturn(users);

        List<User> expected = userService.getAllByDobAsc();

        assertEquals(expected, users);
    }

    @Test
    @DisplayName("Should Sort All Users By JoiningDate Ascending")
    public void shouldGetAllUsersByJoiningAsc() {
        List<User> users = new ArrayList();
        users.add( new User(3L, "UserName1", "Surname1", "9999911111", new Date(1983-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(2L, "UserName2", "Surname2", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false));
        users.add( new User(1L, "UserName3", "Surname3", "9999933333", new Date(1981-01-01),
                new Date(2018-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));

        given(userRepository.findAllByOrderByJoiningDateAsc()).willReturn(users);

        List<User> expected = userService.getAllByJoiningAsc();

        assertEquals(expected, users);
    }
}