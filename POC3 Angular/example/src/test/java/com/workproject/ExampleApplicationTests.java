package com.workproject;

import com.workproject.controller.UserController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ExampleApplicationTests {

	@Autowired
	UserController userController;

	@Test
	public void contextLoads() {
		Assertions.assertTrue(!userController.equals(null));
	}
}
