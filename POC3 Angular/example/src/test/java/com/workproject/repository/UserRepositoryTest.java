package com.workproject.repository;


import com.workproject.model.User;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @BeforeAll
    public void setUp() {
        List<User> users = Arrays.asList(
        new User(1L, "UserName1", "Surname1", "9999911111", new Date(1983-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false),
        new User(2L, "UserName1", "Surname1", "9999922222", new Date(1982-01-01),
                new Date(2017-01-01), "male", "def@xyz.com", "Wall Street NY", 220001, false),
        new User(3L, "UserName1", "Surname1", "9999933333", new Date(1981-01-01),
                new Date(2018-01-01), "male", "geh@xyz.com", "Wall Street NY", 220001, false));
        userRepository.saveAll(users);
    }


    @AfterAll
    public void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    public void findAllByUserName() {
        List<User> all = userRepository.findAllByUserName("UserName1");

        assertEquals(all.get(0).getUserName(), "UserName1");
        assertEquals(3, all.size());
    }

    @Test
    public void findAllBySurname() {
        List<User> all = userRepository.findAllBySurname("Surname1");

        assertEquals(all.get(0).getSurname(), "Surname1");
        assertEquals(3, all.size());
    }

    @Test
    public void findAllByPinCode() {
        List<User> all = userRepository.findAllByPinCode(220001);

        assertEquals(all.get(0).getPinCode(), 220001);
        assertEquals(3, all.size());
    }

    @Test
    public void findAllByOrderByBirthDateAsc() {
        List<User> all = userRepository.findAllByOrderByBirthDateAsc();
        assertEquals(3, all.size());
    }

    @Test
    public void findAllByOrderByJoiningDateAsc() {
        List<User> all = userRepository.findAllByOrderByJoiningDateAsc();
        assertEquals(3, all.size());
    }

    @Test
    public void disableUserById() {
        userRepository.disableUserById(true, 1L);
        assertEquals(true, userRepository.getOne(1L).isDisabled());
    }

    @Test
    public void testCreateReadDelete() {
        User user = new User(1L, "UserName1", "Surname1", "9999911111", new Date(1983-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        userRepository.save(user);
        Assertions.assertTrue(userRepository.findAll().get(0).getUserName().equals("UserName1"));

        userRepository.deleteAll();
        Assertions.assertTrue(userRepository.findAll().isEmpty());
    }
}