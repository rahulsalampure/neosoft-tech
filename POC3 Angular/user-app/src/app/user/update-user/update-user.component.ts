import { BadInput } from './../../common/bad-input';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NameValidators } from '../name.validators';
import { UserModel } from '../user-model';
import { UserPayload } from '../user-payload';
import { AppError } from 'src/app/common/app-error';

@Component({
  selector: 'update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  today = new Date();
  disabled = true;

  usermodel: UserModel;
  userPayload: UserPayload;

  form = new FormGroup({
    id : new FormControl({ value: '', disabled: this.disabled }, Validators.required),
    userName: new FormControl('', [ Validators.required,
                                    Validators.minLength(2),
                                    NameValidators.cannotContainSpace ]),

    surname: new FormControl('', [ Validators.required,
                                   Validators.minLength(2),
                                   NameValidators.cannotContainSpace ]),

    contactNo: new FormControl('', [ Validators.required, 
                                     Validators.pattern("^((?!(0))[0-9]{10})$") ]),

    birthDate: new FormControl('', Validators.required),

    joiningDate: new FormControl('', Validators.required),

    gender: new FormControl('', Validators.required),

    email: new FormControl('', [ Validators.required,
                                 Validators.email,
                                 Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$') ]),

    address: new FormControl('', Validators.required),

    pinCode: new FormControl('', [ Validators.required,
                                   Validators.pattern("^((?!(0))[0-9]{6})$") ])
   });


  ngOnInit(): void {
    var data = history.state;
    delete data['navigationId'];
    this.usermodel = data;
  }

  constructor(private service: UserService, private router: Router) {
    this.userPayload = {
      userName: '',
      surname: '',
      contactNo: '',
      birthDate: new Date(),
      joiningDate: new Date(),
      gender: '',
      email: '',
      address: '',
      pinCode: 0
    }
  }

  updateUser() {
    this.userPayload.userName = this.form.get('userName').value;
    this.userPayload.surname = this.form.get('surname').value;
    this.userPayload.contactNo = this.form.get('contactNo').value;
    this.userPayload.birthDate = this.form.get('birthDate').value;
    this.userPayload.joiningDate = this.form.get('joiningDate').value;
    this.userPayload.gender = this.form.get('gender').value;
    this.userPayload.email = this.form.get('email').value;
    this.userPayload.address = this.form.get('address').value;
    this.userPayload.pinCode = this.form.get('pinCode').value;

    this.service.update(this.userPayload,this.id.value)
    .subscribe(
     updatedUser => {
     console.log(updatedUser);
      if(updatedUser.id == null) {
        throw BadInput;
      }
     this.router.navigateByUrl("/view-user", { state: updatedUser });
    },
    (error: AppError) => {

      if (error instanceof BadInput) {
        throw new BadInput;
      }
      else throw error;
    });
  }

  get id() {
    return this.form.get('id');
  }

  get userName() {
    return this.form.get('userName');
  }

  get surname() {
    return this.form.get('surname');
  }
  
  get contactNo() {
    return this.form.get('contactNo');
  }

  get birthDate() {
    return this.form.get('birthDate');
  }

  get joiningDate() {
    return this.form.get('joiningDate');
  }

  get gender() {
    return this.form.get('gender');
  }

  get email() {
    return this.form.get('email');
  }

  get address() {
    return this.form.get('address');
  }

  get pinCode() {
    return this.form.get('pinCode');
  }  

}
