import { UserModel } from './../user-model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {
  usermodel: UserModel;

  constructor(private router: Router) { }

  ngOnInit(): void {
    var data = history.state;
    delete data['navigationId'];
    this.usermodel = data
    console.log(data);
  }

  goToViewAll() {
    this.router.navigateByUrl('/list-user');
  }

}
