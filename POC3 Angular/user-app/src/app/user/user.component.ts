import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { AppError } from '../common/app-error';
import { BadInput } from '../common/bad-input';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NameValidators } from './name.validators';
import { UserPayload } from './user-payload';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  today = new Date();

  userPayload: UserPayload;

  form = new FormGroup({
                         userName: new FormControl('', [ Validators.required,
                                                         Validators.minLength(2),
                                                         NameValidators.cannotContainSpace ]),

                         surname: new FormControl('', [ Validators.required,
                                                        Validators.minLength(2),
                                                        NameValidators.cannotContainSpace ]),

                         contactNo: new FormControl('', [ Validators.required, 
                                                          Validators.pattern("^((?!(0))[0-9]{10})$") ]),

                         birthDate: new FormControl('', Validators.required),

                         joiningDate: new FormControl('', Validators.required),

                         gender: new FormControl('', Validators.required),

                         email: new FormControl('', [ Validators.required,
                                                      Validators.email,
                                                      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$') ]),
   
                         address: new FormControl('1600 Amphitheatre Pkwy', Validators.required),

                         pinCode: new FormControl('940436', [ Validators.required,
                                                        Validators.pattern("^((?!(0))[0-9]{6})$") ])
                        });
  
  constructor(private service: UserService, private router: Router) {
    this.userPayload = {
      userName: '',
      surname: '',
      contactNo: '',
      birthDate: new Date,
      joiningDate: new Date,
      gender: '',
      email: '',
      address: '',
      pinCode: 0
    }
  }

  ngOnInit(): void {
  }

  
  createUser(){
    this.userPayload.userName = this.form.get('userName').value;
    this.userPayload.surname = this.form.get('surname').value;
    this.userPayload.contactNo = this.form.get('contactNo').value;
    this.userPayload.birthDate = this.form.get('birthDate').value;
    this.userPayload.joiningDate = this.form.get('joiningDate').value;
    this.userPayload.gender = this.form.get('gender').value;
    this.userPayload.email = this.form.get('email').value;
    this.userPayload.address = this.form.get('address').value;
    this.userPayload.pinCode = this.form.get('pinCode').value;


    this.service.create(this.userPayload)
    .subscribe(
   newUser => {
     if(newUser.id == null) {
       throw BadInput;
     }
    this.router.navigateByUrl("/view-user", { state: newUser });
    },
   (error: AppError) => {

     if (error instanceof BadInput) {
       throw new BadInput;
     }
     else throw error;
   });
  }

  get userName() {
    return this.form.get('userName');
  }

  get surname() {
    return this.form.get('surname');
  }
  
  get contactNo() {
    return this.form.get('contactNo');
  }

  get birthDate() {
    return this.form.get('birthDate');
  }

  get joiningDate() {
    return this.form.get('joiningDate');
  }

  get gender() {
    return this.form.get('gender');
  }

  get email() {
    return this.form.get('email');
  }

  get address() {
    return this.form.get('address');
  }

  get pinCode() {
    return this.form.get('pinCode');
  }
}
