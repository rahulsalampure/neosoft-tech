import { UserModel } from './../user-model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from 'src/app/services/user.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { UserPayload } from '../user-payload';
import { Router } from '@angular/router';

@Component({
  selector: 'list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
  result;
  users: any[];
  userPayload: UserPayload;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  post =     {
    "userName": "Cal",
    "surname": "Newport",
    "contactNo": 8871112233,
    "birthDate": "1921-01-01T00:00:00.000+00:00",
    "joiningDate": "2016-01-01T00:00:00.000+00:00",
    "gender": "MALE",
    "email": "emf@xyz.com",
    "address": "Wall Street NY",
    "pinCode": 220001
};

public displayedColumns = ['id', 'userName', 'surname','email', 'contactNo', 'birthDate',
                           'joiningDate', 'gender', 'address', 'pinCode', 'details', 'update','disable', 'delete'];

  public dataSource = new MatTableDataSource<UserModel>();

  constructor(private service: UserService, private router: Router) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  updateUser(id: number) {
    this.result = this.users.filter(obj => {
      return obj.id === id
    });
    var newResult = this.result[0];
    console.log(newResult);
    this.router.navigateByUrl("/update-user", { state: newResult });
  }

  getAllUsers() {
    this.getAllService('');
  }

  getAllService(link: string){
    this.service.getAll(link)
    .subscribe( userData => {    
      this.users = userData; 
      this.dataSource.data = userData; 
    });
  }

  disableUserByID(id: number) {
    this.service.disable(id)
    .subscribe( users => users );
  }

  deleteUser(id: number) {
    this.service.deleteUserByID(id)
    .subscribe(
      deletedUser => {
        deletedUser;
        this.users.splice(this.users.findIndex(x => x.id === id), 1);
        this.dataSource.data = this.users;
      },
    error => {
      alert('An unexpected error occurred');
      console.log(error);
    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  emptyFunction() { }

  // getAllUsersByDOBAsc() {
  //   this.getAllService('/list-by-dob');
  // }

  // getAllUsersByJoinAsc() {
  //   this.getAllService('/list-by-joining-date');
  // }

  // getAllUsersByName(name: string) {
  //   this.getAllService('/username/'+name);
  // }

  // getAllUsersBySurname(surname: string) {
  //   this.getAllService('/surname/'+surname);
  // }

  // getAllUsersByPincode(pincode: number) {
  //   this.getAllService('/pincode/'+pincode);
  // }  
}
