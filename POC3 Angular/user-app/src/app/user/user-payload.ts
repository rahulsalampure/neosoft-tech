
export class UserPayload {
    userName: string;
    surname: string;
    contactNo: string;
    birthDate: Date;
    joiningDate: Date;
    gender: string;
    email: string;
    address: string;
    pinCode: number;
}