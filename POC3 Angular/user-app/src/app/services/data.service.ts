import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AppError } from '../common/app-error';
import { BadInput } from '../common/bad-input';
import { NotFoundError } from '../common/not-found-error';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private url: string,private http: HttpClient) { }

  getAll(link) {
    return this.http.get<any>(this.url+link)
    .pipe(map(response=>response),catchError(this.handleError));
  }

  create(resource) {
    return this.http.post<any>(this.url, resource)
    .pipe(map(response=>response),catchError(this.handleError));
  }

  update(resource,id) {
    return this.http.put<any>(this.url + '/' + id, resource)
    .pipe(map(response=>response),catchError(this.handleError));
  }

  disable(id) {
    return this.http.put(this.url + '/disable/' + id ,{})
    .pipe(map(response=>response),catchError(this.handleError));
  }

  deleteUserByID(id) { 
    return this.http.delete(this.url + '/' + id, {})
    .pipe(map(response=>response), catchError(this.handleError));
  }

  private handleError(error: Response) {
    if (error.status === 400)
      return throwError(new BadInput(error));
    
    if (error.status === 404)
      return throwError(new NotFoundError());
      
    return throwError(new AppError(error));
  }
}
