import { UpdateUserComponent } from './user/update-user/update-user.component';
import { HomeComponent } from './home/home.component';
import { ViewUserComponent } from './user/view-user/view-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ListUserComponent } from './user/list-user/list-user.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'user', component: UserComponent,
  },
  {
    path: 'view-user', component: ViewUserComponent
  },
  {
    path: 'list-user', component: ListUserComponent
  },
  {
    path: 'update-user', component: UpdateUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
