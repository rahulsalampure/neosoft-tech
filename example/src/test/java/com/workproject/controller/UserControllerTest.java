package com.workproject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.workproject.dto.UserRequestDTO;
import com.workproject.model.User;
import com.workproject.service.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static java.util.Arrays.asList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    User user1;

    @BeforeEach
    void setUp() {
        user1 = new User(1L, "UserName1", "Surname1", "9999911111", new Date(1980-01-01),
                new Date(2017-01-01), "male", "fds@xyz.com", "Wall Street NY", 220001, false);
    }

    @Test
    @DisplayName("Should Save User When making POST request to endpoint - /user")
    void shouldCreateNewUser() throws Exception {

        UserRequestDTO userRequestDTO = new UserRequestDTO( "UserName1", "Surname1", "9999911111", new Date(1980-01-01),
                new Date(2017-01-01), "male", "fds@xyz.com", "Wall Street NY", 220001);

        Mockito.when(userService.save(userRequestDTO)).thenReturn(user1);

        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequestDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.size()", Matchers.is(10)))
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.surname", Matchers.is("Surname1")));
    }

    @Test
    @DisplayName("Should List All Users by UserName When making GET request to endpoint - /user/username/UserName1")
    public void shouldGetAllUsersByName() throws Exception {
        User user2 = new User(2L, "UserName1", "Surname2", "1111199999", new Date(1971-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        Mockito.when(userService.getAllByUsername("UserName1")).thenReturn(asList(user1, user2));

        mockMvc.perform(get("/user/username/UserName1"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[0].userName", Matchers.is("UserName1")))
                .andExpect(jsonPath("$[1].userName", Matchers.is("UserName1")))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)));
    }

    @Test
    @DisplayName("Should List All Users by SurName When making GET request to endpoint - /user/surname/Surname1")
    public void shouldGetAllUsersBySurName() throws Exception {
        User user2 = new User(2L, "UserName2", "Surname1", "1111199999", new Date(1971-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        Mockito.when(userService.getAllBySurname("Surname1")).thenReturn(asList(user1, user2));

        mockMvc.perform(get("/user/surname/Surname1"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[0].surname", Matchers.is("Surname1")))
                .andExpect(jsonPath("$[1].surname", Matchers.is("Surname1")))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)));
    }

    @Test
    @DisplayName("Should List All Users by PinCode When making GET request to endpoint - /user/pincode/220001")
    public void shouldGetAllUsersByPinCode() throws Exception {
        User user2 = new User(2L, "UserName2", "Surname2", "1111199999", new Date(1971-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        Mockito.when(userService.getAllByPinCode(220001)).thenReturn(asList(user1, user2));

        mockMvc.perform(get("/user/pincode/220001"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[0].pinCode", Matchers.is(220001)))
                .andExpect(jsonPath("$[1].pinCode", Matchers.is(220001)))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)));
    }

    @Test
    @DisplayName("Should Sort All Users by DateOfBirth Ascending When making GET request to endpoint - /user/list-by-dob")
    public void shouldGetUsersByDobAscending() throws Exception {
        User user2 = new User(2L, "UserName1", "Surname2", "1111199999", new Date(1971-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        Mockito.when(userService.getAllByDobAsc()).thenReturn(asList(user2, user1));

        mockMvc.perform(get("/user/list-by-dob"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(2)))
                .andExpect(jsonPath("$[0].surname", Matchers.is("Surname2")))
                .andExpect(jsonPath("$[1].surname", Matchers.is("Surname1")))
                .andExpect(jsonPath("$[1].id", Matchers.is(1)));
    }

    @Test
    @DisplayName("Should Sort All Users by Joining-date When making GET request to endpoint - /user/list-by-joining-date")
    public void getUsersByJoiningDateAscending() throws Exception {
        User user2 = new User(2L, "UserName1", "Surname2", "1111199999", new Date(1971-01-01),
                new Date(2015-01-01), "male", "abc@xyz.com", "Wall Street NY", 220001, false);

        Mockito.when(userService.getAllByJoiningAsc()).thenReturn(asList(user2, user1));

        mockMvc.perform(get("/user/list-by-joining-date"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(2)))
                .andExpect(jsonPath("$[0].surname", Matchers.is("Surname2")))
                .andExpect(jsonPath("$[1].surname", Matchers.is("Surname1")))
                .andExpect(jsonPath("$[1].id", Matchers.is(1)));
    }

    @Test
    @DisplayName("Should Update User by Id PUT request to endpoint - /user/1")
    public void shouldUpdateUser() throws Exception {
        UserRequestDTO userRequestDTO = new UserRequestDTO("UserName1", "Surname1", "9999911111", new Date(1980 - 01 - 01),
                new Date(2017 - 01 - 01), "male", "fds@xyz.com", "Wall Street NY", 220001);

        Mockito.when(userService.updateUser(userRequestDTO, user1.getId())).thenReturn(user1);

        mockMvc.perform(put("/user/{id}", user1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequestDTO)))
                .andExpect(status().is(202));
    }

    @Test
    @DisplayName("Should Not Update User by Id PUT request to endpoint - /user/1")
    public void shouldNotUpdateUser() throws Exception {
        Long id = 1L;
        UserRequestDTO userRequestDTO = new UserRequestDTO("UserName1", "Surname1", "9999911111", new Date(1980 - 01 - 01),
                new Date(2017 - 01 - 01), "male", "fds@xyz.com", "Wall Street NY", 220001);

        User user = new User(null, null, null, null, null,
                null, null, null, null, null, true);

        Mockito.when(userService.updateUser(userRequestDTO, id)).thenReturn(user);

        mockMvc.perform(put("/user/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequestDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Should Disable User by Id PUT request to endpoint - /user/disable/1")
    public void shouldDisableUser() throws Exception {

        UserRequestDTO userRequestDTO = new UserRequestDTO( "UserName1", "Surname1", "9999911111", new Date(1980-01-01),
                new Date(2017-01-01), "male", "fds@xyz.com", "Wall Street NY", 220001);

        Mockito.when(userService.updateUser(userRequestDTO, user1.getId())).thenReturn(null);

        mockMvc.perform(put("/user/disable/{id}", user1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequestDTO)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Delete User by Id DELETE request to endpoint - /delete/1")
    void shouldDeleteUser() throws Exception {
        Long userId = 1L;

        Mockito.when(userService.delete(userId)).thenReturn(null);

        mockMvc.perform(delete("/user/{userId}", user1.getId())).andExpect(status().isOk());
    }
}