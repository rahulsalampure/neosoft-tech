package com.workproject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "uuser",uniqueConstraints = {@UniqueConstraint (columnNames = "contact_no"),
                                           @UniqueConstraint (columnNames = "email")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seq")
    @SequenceGenerator(name = "seq",sequenceName = "uuser_id_seq")
    private Long id;

    @NotBlank(message = "Username is required")
    @Column(name="username")
    private String userName;

    @NotBlank(message = "Surname is required")
    private String surname;

    @Pattern(regexp="^((?!(0))[0-9]{10})$",message="ContactNo must be of 10 digit")
    @Column(name="contact_no",unique = true)
    private String contactNo;

    @NotNull
    @Past
    private Date birthDate;

    @NotNull
    private Date joiningDate;

    @NotEmpty(message = "Gender should not be empty")
    private String gender;

    @NotEmpty(message = "Email is required")
    @Email(message = "Email should be valid")
    @Column(name="email",unique = true)
    private String email;

    @NotBlank(message = "Address is required")
    private String address;

    @NotNull
    @Column(name="pincode")
    @Max(value = 999999, message = "Invalid Pincode")
    private Integer pinCode;

    private boolean disabled;

}
