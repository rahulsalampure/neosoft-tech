package com.example.client;

import com.example.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "shop-api")
public interface RestClient {

    @GetMapping("/customers/customer")
    ResponseEntity<List<CustomerDTO>> getCustomer();

    @GetMapping("/customers/customer/{id}")
    ResponseEntity<CustomerDTO> getCustomerById(@PathVariable("id") Long id);

    @PostMapping("/customers/customer")
    ResponseEntity<CustomerDTO> registerCustomer(@RequestBody CustomerDTO customerDTO);

    @PutMapping("/customers/customer/{id}")
    ResponseEntity<CustomerDTO> updateCustomer(@RequestBody CustomerDTO customerDTO,@PathVariable Long id);

    @DeleteMapping("/customers/customer/{id}")
    ResponseEntity delete(@PathVariable("id") Long id);
}
