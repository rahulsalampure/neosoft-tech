package com.example.controller;

import com.example.client.RestClient;
import com.example.dto.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class HomeController {

    @Autowired
    final RestClient restClient;

    public HomeController(RestClient restClient) {
        this.restClient = restClient;
    }

    @GetMapping("/customer")
    public ResponseEntity<List<CustomerDTO>> getUser() {
        return restClient.getCustomer();
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<CustomerDTO> getUserById(@PathVariable("id") Long id) {
        return restClient.getCustomerById(id);
    }

    @PostMapping("/customer")
    public ResponseEntity<CustomerDTO> registerUser(@RequestBody CustomerDTO customerDTO) {
        return restClient.registerCustomer(customerDTO);
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<CustomerDTO> updateUser(@RequestBody CustomerDTO customerDTO,@PathVariable Long id) {
        return restClient.updateCustomer(customerDTO, id);
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") Long id) {
        return restClient.delete(id);
    }
}
