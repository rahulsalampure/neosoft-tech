package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    private Long customerId;

    private String firstName;

    private String lastName;

    private String mobileNo;

    private String email;

    private String address;

    private List<ProductDTO> productList = new ArrayList<>();
}
