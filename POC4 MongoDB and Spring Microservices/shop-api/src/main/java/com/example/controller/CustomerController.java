package com.example.controller;

import com.example.dto.CustomerDTO;
import com.example.model.Customer;
import com.example.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customer")
    public ResponseEntity<List<CustomerDTO>> getCustomer(){
        return reduce(customerService.findAllCustomer());
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<CustomerDTO> getCustomerById(@PathVariable("id") Long id){
        Customer customer = customerService.findCustomerById(id);
        if(customer.getCustomerId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(transform(customer), HttpStatus.OK);
    }

    @PostMapping("/customer")
    public ResponseEntity<CustomerDTO> registerCustomer(@RequestBody CustomerDTO customerDTO) {
        return new ResponseEntity<>(transform(customerService.save(customerDTO)), HttpStatus.CREATED);
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody CustomerDTO customerDTO,@PathVariable Long id) {
        Customer customer = customerService.updateCustomer(customerDTO, id);
        if(customer.getCustomerId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(transform(customer), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        return customerService.delete(id);
    }

    public static ResponseEntity reduce(List<Customer> customers) {
        List<CustomerDTO> customerDTOList = customers.stream().map(CustomerController::transform).collect(toList());
        return customerDTOList.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND):
                new ResponseEntity<>(customerDTOList, HttpStatus.OK);
    }

    public static CustomerDTO transform(final Customer customer) {

        final CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCustomerId(customer.getCustomerId());
        customerDTO.setFirstName(customer.getFirstName());
        customerDTO.setLastName(customer.getLastName());
        customerDTO.setMobileNo(customer.getMobileNo());
        customerDTO.setEmail(customer.getEmail());
        customerDTO.setAddress(customer.getAddress());
        customerDTO.setProductList(customer.getProductList());

        return customerDTO;
    }
}
