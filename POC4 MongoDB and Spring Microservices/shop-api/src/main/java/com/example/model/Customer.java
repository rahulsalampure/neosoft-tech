package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "Shop")
public class Customer {

    @Id
    private Long customerId;

    private String firstName;

    private String lastName;

    @Pattern(regexp="^((?!(0))[0-9]{10})$",message="mobileNo must be of 10 digit")
    private String mobileNo;

    @NotEmpty(message = "Email is required")
    @Email(message = "Email should be valid")
    private String email;

    private String address;

    private List<Product> productList = new ArrayList<>();
}
