package com.example.service;

import com.example.dto.CustomerDTO;
import com.example.model.Customer;
import com.example.repository.ShopRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Log4j2
@AllArgsConstructor
public class CustomerService {

    private static final Logger logger = LogManager.getLogger(CustomerService.class.getName());

    @Autowired
    private ShopRepository shopRepository;

    @Transactional(readOnly = true)
    public List<Customer> findAllCustomer(){
        return shopRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Customer findCustomerById(final Long id){
        return shopRepository.existsById(id) ? shopRepository.findById(id).get() : new Customer();
    }

    @Transactional
    public Customer save(CustomerDTO customerDTO) {
        try {
            return shopRepository.save(copy(customerDTO));
        } catch (Exception exception) {
            logger.error("Error Occurred!!!: {}", exception);
        }
        return new Customer();
    }

    @Transactional
    public Customer updateCustomer(CustomerDTO customerDTO, Long id) {
        try {
            if (!shopRepository.existsById(id)) {
                return new Customer();
            }
            return shopRepository.save(copy(customerDTO));
        } catch (Exception exception) {
            logger.error("Error Occurred!!!: {}", exception);
        }
        return new Customer();
    }

    @Transactional
    public ResponseEntity delete(Long id) {
        if(!shopRepository.existsById(id)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        shopRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public static Customer copy(final CustomerDTO customerDTO) {
        final Customer customer = new Customer();
        customer.setCustomerId(customerDTO.getCustomerId());
        customer.setFirstName(customerDTO.getFirstName());
        customer.setLastName(customerDTO.getLastName());
        customer.setMobileNo(customerDTO.getMobileNo());
        customer.setEmail(customerDTO.getEmail());
        customer.setAddress(customerDTO.getAddress());
        customer.setProductList(customerDTO.getProductList());

        return customer;
    }
}
