package com.example.dto;

import com.example.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    private Long customerId;

    private String firstName;

    private String lastName;

    private String mobileNo;

    private String email;

    private String address;

    private List<Product> productList = new ArrayList<>();
}
