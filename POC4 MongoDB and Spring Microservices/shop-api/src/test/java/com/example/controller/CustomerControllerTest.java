package com.example.controller;

import com.example.dto.CustomerDTO;
import com.example.model.Customer;
import com.example.model.Product;
import com.example.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static java.util.Arrays.asList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CustomerController.class)
class CustomerControllerTest {

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    Customer customer1;

    @BeforeEach
    void setUp() {
        customer1 = new Customer(1L, "FirstName1", "Surname1", "9999911111",
                "abc@xyz.com", "Wall Street NY", Arrays.asList(
                        new Product("HP",100.00,"Computer"),
                        new Product("Samsung",50.00,"Mobile")));
    }

    @Test
    @DisplayName("Should Save Customer When making POST request to endpoint - /customers/customer")
    void shouldCreateNewCustomer() throws Exception {

        CustomerDTO customerDTO = new CustomerDTO(1L, "FirstName1", "Surname1",
                "9999911111", "abc@xyz.com", "Wall Street NY", Arrays.asList(
                               new Product("HP",100.00,"Computer"),
                               new Product("Samsung",50.00,"Mobile")));

        Mockito.when(customerService.save(customerDTO)).thenReturn(customer1);

        mockMvc.perform(post("/customers/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customerDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.size()", Matchers.is(7)))
                .andExpect(jsonPath("$.customerId", Matchers.is(1)))
                .andExpect(jsonPath("$.lastName", Matchers.is("Surname1")));
    }

    @Test
    @DisplayName("Should List All customer When making GET request to endpoint - /customers/customer")
    public void shouldGetAllCustomer() throws Exception {
        Customer customer2 = new Customer(2L, "FirstName1", "Surname2",
                  "9999922222", "def@xyz.com", "Wall Street NY", Arrays.asList(
                                 new Product("HP",100.00,"Computer"),
                                 new Product("Samsung",50.00,"Mobile")));

        Mockito.when(customerService.findAllCustomer()).thenReturn(asList(customer1, customer2));

        mockMvc.perform(get("/customers/customer"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].customerId", Matchers.is(1)))
                .andExpect(jsonPath("$[0].firstName", Matchers.is("FirstName1")))
                .andExpect(jsonPath("$[1].firstName", Matchers.is("FirstName1")))
                .andExpect(jsonPath("$[1].customerId", Matchers.is(2)));
    }

    @Test
    @DisplayName("Should Get Customer by Id When making GET request to endpoint - /customers/customer/1")
    public void shouldGetCustomerById() throws Exception {
        Long userId = 1L;

        Mockito.when(customerService.findCustomerById(userId)).thenReturn(customer1);

        mockMvc.perform(delete("/customers/customer/{userId}", customer1.getCustomerId())).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Update Customer by Id PUT request to endpoint - /customers/customer/1")
    public void shouldUpdateCustomer() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO(1L, "FirstName1", "Surname1",
                        "9999911111", "abc@xyz.com", "Wall Street NY", Arrays.asList(
                                  new Product("HP",100.00,"Computer"),
                                  new Product("Samsung",50.00,"Mobile")));

        Mockito.when(customerService.updateCustomer(customerDTO, customer1.getCustomerId())).thenReturn(customer1);

        mockMvc.perform(put("/customers/customer/{id}", customer1.getCustomerId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customerDTO)))
                .andExpect(status().is(202));
    }

    @Test
    @DisplayName("Should Not Update Customer by Id PUT request to endpoint - /customers/customer/1")
    public void shouldNotUpdateCustomer() throws Exception {
        Long id = 1L;
        CustomerDTO customerDTO = new CustomerDTO(1L, "FirstName1", "Surname1",
                        "9999911111", "abc@xyz.com", "Wall Street NY", Arrays.asList(
                                      new Product("HP",100.00,"Computer"),
                                      new Product("Samsung",50.00,"Mobile")));

        Customer customer = new Customer(null, null, null, null, null,
                                            null, null);

        Mockito.when(customerService.updateCustomer(customerDTO, id)).thenReturn(customer);

        mockMvc.perform(put("/customers/customer/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customerDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Should Delete Customer by Id DELETE request to endpoint - /customers/customer/1")
    void shouldDeleteUser() throws Exception {
        Long userId = 1L;

        Mockito.when(customerService.delete(userId)).thenReturn(null);

        mockMvc.perform(delete("/customers/customer/{userId}", customer1.getCustomerId())).andExpect(status().isOk());
    }
}