package com.example.repository;

import com.example.model.Customer;
import com.example.model.Product;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DataMongoTest
class ShopRepositoryTest {

    @Autowired
    private ShopRepository shopRepository;

    @BeforeAll
    public void setUp() {
        List<Customer> customers = Arrays.asList(
            new Customer(1L, "FirstName1", "Surname1", "9999911111",
                    "abc@xyz.com", "Wall Street NY", Arrays.asList(
                            new Product("HP",100.00,"Computer"),
                            new Product("Samsung",50.00,"Mobile"))),
            new Customer(2L, "FirstName2", "Surname2", "9999922222",
                    "def@xyz.com", "Wall Street NY", Arrays.asList(
                            new Product("HP",100.00,"Computer"),
                            new Product("Samsung",50.00,"Mobile"))),
           new Customer(3L, "FirstName3", "Surname3", "9999933333",
                   "ghi@xyz.com", "Wall Street NY", Arrays.asList(
                            new Product("HP",100.00,"Computer"),
                            new Product("Samsung",50.00,"Mobile"))));
        shopRepository.saveAll(customers);
    }


    @AfterAll
    public void tearDown() {
        shopRepository.deleteAll();
    }

    @Test
    public void findAllCustomer() {
        List<Customer> all = shopRepository.findAll();
        assertEquals(3, all.size());
    }

    @Test
    public void findCustomerById() {
        Customer customer = new Customer(4L, "FirstName4", "Surname4", "9999944444",
                "jkl@xyz.com", "Wall Street NY", Arrays.asList(
                         new Product("HP",100.00,"Computer"),
                         new Product("Samsung",50.00,"Mobile")));

        shopRepository.save(customer);
        assertEquals("FirstName4", shopRepository.findById(4L).get().getFirstName());
    }

    @Test
    public void testCreateReadDelete() {
        Customer customer = new Customer(5L, "FirstName5", "Surname5", "9999955555",
                "mno@xyz.com", "Wall Street NY", Arrays.asList(
                         new Product("HP",100.00,"Computer"),
                         new Product("Samsung",50.00,"Mobile")));

        shopRepository.save(customer);
        Assertions.assertTrue(shopRepository.findAll().get(3).getFirstName().equals("FirstName5"));

        shopRepository.deleteAll();
        Assertions.assertTrue(shopRepository.findAll().isEmpty());
    }
}
