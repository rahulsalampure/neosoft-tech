package com.example.service;

import com.example.dto.CustomerDTO;
import com.example.model.Customer;
import com.example.model.Product;
import com.example.repository.ShopRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    private ShopRepository shopRepository;

    @InjectMocks
    private CustomerService customerService;

    @Captor
    private ArgumentCaptor<Customer> postArgumentCaptor;

    @Test
    @DisplayName("Should Save Customers")
    public void shouldSaveCustomers() {
        CustomerDTO customerDTO = new CustomerDTO(1L, "FirstName1", "Surname1",
                "9999911111", "abc@xyz.com", "Wall Street NY", Arrays.asList(
                          new Product("HP",100.00,"Computer"),
                          new Product("Samsung",50.00,"Mobile")));

        customerService.save(customerDTO);

        Mockito.verify(shopRepository, times(1)).save(postArgumentCaptor.capture());

        Assertions.assertThat(postArgumentCaptor.getValue().getFirstName()).isEqualTo("FirstName1");
        Assertions.assertThat(postArgumentCaptor.getValue().getLastName()).isEqualTo("Surname1");
    }

    @Test
    @DisplayName("Should Get All Customers")
    public void shouldGetAllCustomers() {
        List<Customer> customers = new ArrayList();
                customers.add(new Customer(1L, "FirstName1", "Surname1", "9999911111",
                "abc@xyz.com", "Wall Street NY", Arrays.asList(
                        new Product("HP",100.00,"Computer"),
                        new Product("Samsung",50.00,"Mobile"))));
                customers.add(new Customer(2L, "FirstName2", "Surname2", "9999922222",
                        "def@xyz.com", "Wall Street NY", Arrays.asList(
                        new Product("HP",100.00,"Computer"),
                        new Product("Samsung",50.00,"Mobile"))));
                customers.add(new Customer(3L, "FirstName3", "Surname3", "9999933333",
                "ghi@xyz.com", "Wall Street NY", Arrays.asList(
                        new Product("HP",100.00,"Computer"),
                        new Product("Samsung",50.00,"Mobile"))));

        given(shopRepository.findAll()).willReturn(customers);

        List<Customer> expected = customerService.findAllCustomer();

        assertEquals(expected, customers);
    }

    @Test
    @DisplayName("Should Delete Customer By Id")
    public void shouldDeleteCustomerById() {
        Customer customer = new Customer(9L, "FirstName9", "Surname9",
                "9999977777", "wes@xyz.com", "Wall Street NY", Arrays.asList(
                new Product("HP",100.00,"Computer"),
                new Product("Samsung",50.00,"Mobile")));
        shopRepository.save(customer);
        shopRepository.deleteById(customer.getCustomerId());
        Optional optional = shopRepository.findById(customer.getCustomerId());
        assertEquals(Optional.empty(), optional);
    }
}