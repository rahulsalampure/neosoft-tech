package com.housiegame;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Ticket {

    private int ticketID;
    private String[][] numbers;

    public Ticket(int xRows, int yColumns) {
        ticketID = -1;
        numbers = new String[xRows][yColumns];

        for (int i = 0; i<xRows ; i++) {
            for (int j = 0; j<yColumns ; j++) {
                numbers[i][j] = null;
            }
        }
    }

    public int getNumber(int row, int col) {
        return numbers[row][col] != null ? Integer.parseInt(numbers[row][col]) : -1;
    }

    public void setNumber(int row, int col, int number) {
        numbers[row][col] = number == -1 ? null : "" + number;
    }

    public String[] getRowNumbers(int row) {
        return (row < 0 || row >= 3) ? null : numbers[row];
    }

    public String[] getColumnNumbers(int col) {
        if ( col < 0 || col >= 9 )
            return null;
        String cols[] = new String[3];
        cols[0] = numbers[0][col];
        cols[1] = numbers[1][col];
        cols[2] = numbers[2][col];

        return cols;
    }

    public int getNumberCountForRow(int row) {
        String[] rowNumbers = getRowNumbers(row);
        return (int) IntStream.range(0, rowNumbers.length).filter(j -> rowNumbers[j] != null).count();
    }

    public int[] getEmptyColumns() {
        int index = 0,size=9;
        int emptyCols[] = new int[size];

        for (int i = 0; i<size ; i++) {
            String cols[] = getColumnNumbers(i);
            if ( cols[0] == null && cols[1] == null && cols[2] == null)
                emptyCols[index++] = i;
        }
        return Arrays.stream(emptyCols, 0, index).toArray();
    }

    public int[] getFullColumns() {
        int index = 0,size=9;;
        int fullCols[] = new int[size];

        for (int i = 0; i<size ; i++) {
            String cols[] = getColumnNumbers(i);
            if( cols[0] != null && cols[1] != null && cols[2] != null )
                fullCols[index++] = i;
        }
        return Arrays.stream(fullCols, 0, index).toArray();
    }

    public boolean isNumberValid(int col, int number) {
        return IntStream.range(0, 3).filter(i -> numbers[i][col] != null).noneMatch(i -> numbers[i][col].equals("" + number));
    }

    public void finalizeTicket() {
        int size=9;
        for (int i = 0; i <size; i++) {
            String[] cols = getColumnNumbers(i);
            for (int j = 0; j <cols.length-1; j++) {
                for (int k = j+1; k < cols.length; k++) {
                    if ( cols[j] == null || cols[k] == null )
                        continue;
                    else if (Integer.parseInt(cols[j]) > Integer.parseInt(cols[k])) {
                        String temp = cols[k];
                        setNumber(j, i, Integer.parseInt(cols[k]));
                        setNumber(k, i, Integer.parseInt(cols[j]));
                        cols[j] = cols[k];
                        cols[k] = temp;
                    }
                }
            }
        }
    }

    public String toString(){
        String result = " --- --- --- --- --- --- --- --- ---\n" +
                       (" Ticket ID: " + this.ticketID + "\n\n") +
                        " --- --- --- --- --- --- --- --- ---\n";
        for ( int i=0; i<3; i++ ){
            for ( int j=0; j<9; j++ ){
                result += numbers[i][j] == null ? "|   " :
                          numbers[i][j].length() == 1 ? "|  " + numbers[i][j] : "| " + numbers[i][j];
            }
            result += "|\n --- --- --- --- --- --- --- --- ---\n";
        }
        return result;
    }


    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public String[][] getNumbers() {
        return numbers;
    }

    public void setNumbers(String[][] numbers) {
        this.numbers = numbers;
    }

}
