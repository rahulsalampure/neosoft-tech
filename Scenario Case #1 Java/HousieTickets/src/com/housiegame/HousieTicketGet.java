package com.housiegame;

import java.util.Random;
import java.util.stream.IntStream;

public class HousieTicketGet {

    static int ticketCounter = 1;

    public Ticket generateTicket(int xRows,int yColumns) {
        Ticket ticket = new Ticket(xRows, yColumns);
        ticket.setTicketID(ticketCounter++);

        Random randomNumber = new Random();

        int lastColMax = -1,
            colMax = randomNumber.nextInt(3);

        for (int i = 0; i<yColumns; i++) {
            while(lastColMax == colMax)
                colMax = randomNumber.nextInt(3);

            lastColMax = colMax;

            for (int j = 0; j < colMax; j++) {
                int number = getRandomNumber(i, ticket);
                int row = randomNumber.nextInt(3);

                while(ticket.getNumber(row, i) != -1)
                    row = randomNumber.nextInt(3);
                ticket.setNumber(row, i, number);
            }
        }
        if(!isTicketFilled(ticket))
            fillTicket(ticket);
        adjustTicket(ticket);
        ticket.finalizeTicket();
        return ticket;
    }

    public void adjustTicket(Ticket ticket) {
        int emptyCols[] = ticket.getEmptyColumns(),
             fullCols[] = ticket.getFullColumns();

        if(emptyCols.length == 0)
            return;

        if (emptyCols.length <= fullCols.length) {
            IntStream.range(0, emptyCols.length).forEachOrdered(i -> {
                int row = (new Random()).nextInt(3),
                 number = getRandomNumber(emptyCols[i], ticket);
                ticket.setNumber(row, fullCols[i], -1);
                ticket.setNumber(row, emptyCols[i], number);
            });
        }
    }

    public void fillTicket(Ticket ticket) {
        Random randomNumber = new Random();
        int iterations1, iterations2,size=3;
        iterations1 = iterations2 = 0;

        for (int i = 0; i<size; i++) {
            boolean flag = false;

            if( ticket.getNumberCountForRow(i)>=6) {
                while( ticket.getNumberCountForRow(i) != 5) {
                    int col = randomNumber.nextInt(9);
                    if( ticket.getNumber(i, col) != -1)
                        ticket.setNumber(i, col, -1);
                }
                continue;
            }
            while( ticket.getNumberCountForRow( i ) <= 4 ){
                int col = randomNumber.nextInt( 9 );
                while( ticket.getNumber( i, col ) != -1 ){
                    iterations2++;
                    col = randomNumber.nextInt( 9 );
                    if( iterations2 >= 100 ){
                        flag = true;
                        break;
                    }
                }
                if( flag ){
                    col = -1;
                    for( int col1=0; col<9; col1++ )
                        if( ticket.getNumber( i, col1 ) == -1 ){
                            col = col1;
                            break;
                        }
                    if (col == -1)
                    {
                        i++;
                        continue;
                    }
                }
                int number = getRandomNumber( col, ticket );
                ticket.setNumber( i, col, number );
                iterations1++;
            }
        }
    }

    boolean isTicketFilled(Ticket ticket) {
        return IntStream.range(0, 3).map(ticket::getNumberCountForRow).noneMatch(count -> count != 5) && true;
    }

    public int getRandomNumber(int col, Ticket ticket) {
        int number = 0,iterations = 0;
        Random randomNumber = new Random();
        do{
            number = randomNumber.nextInt( (col*10)+11 );
            iterations++;
        }while( (number <= col*10 || number >= (col*10)+11) || !(ticket.isNumberValid( col, number )) );
        return number;
    }
}
