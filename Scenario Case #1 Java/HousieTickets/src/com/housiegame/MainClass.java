package com.housiegame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainClass {
    private static String value;
    private static int xRows, yColumns;
    private static HousieTicketGet housieTicketGet = new HousieTicketGet();
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


    private static void readerMethod() throws IOException {
        try {
            System.out.println("Housie Ticket Generator\n------------------------" +
                    "\nHousie Ticket having 3 rows and 9 columns " +
                    "\n------------------------------------------" +
                    "\n(Valid Ticket rows:3 & columns: 9):" +
                    "\nEnter row numbers for Housie Ticket: ");
            xRows = Integer.parseInt(bufferedReader.readLine());
            System.out.println("Enter columns numbers for Housie Ticket:");
            yColumns = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private static void outputMethod() throws IOException {
        if (xRows != 3 && yColumns != 9) {
            System.out.println("Invalid rows & columns for Housie ticket!!!");
            System.exit(0);
        }
        do {
            System.out.println(
                    housieTicketGet.generateTicket(xRows, yColumns) + "\nPress Z to exit & any to continue...  "
            );
            value = bufferedReader.readLine();
        } while (!value.equalsIgnoreCase("Z"));
        bufferedReader.close();
    }

    public static void main(String[] args) throws IOException {
        MainClass.readerMethod();
        MainClass.outputMethod();
    }
}
