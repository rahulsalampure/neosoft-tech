/* Title: Create tables and Insert data script  

   Scenario - Case #2
Write a query given the following data model:
Query: Get list of Posts with latest 10 comments of each post authored by 'James Bond' 

Language used: PostgreSQL

Dummy data from: https://mockaroo.com

Result Query: 

SELECT t.r as row_no,
       t.aid as author_id ,
       t.aname as author_name ,
       t.pid as post_id,
       t.pname as post_name,
       t.created_on,
       t.content as comments,
       t.uname as username,
       t.commented_on
FROM (SELECT *, ROW_NUMBER () OVER (
        PARTITION BY post.pid
        ORDER BY comment.commented_on DESC) AS r
      FROM post
        INNER JOIN author ON post.author_id = author.aid
        INNER JOIN comment ON post.pid = comment.post_id
        INNER JOIN uuser on uuser.uid = comment.user_id
    WHERE author.aname='James Bond') AS t WHERE t.r <= 10;
*/

CREATE TABLE author (
  aid INT PRIMARY KEY,
  aname VARCHAR(100)
);

CREATE TABLE post (
    pid INT PRIMARY KEY,
    pname VARCHAR(100),
    author_id INT NOT NULL,
    FOREIGN KEY(author_id) REFERENCES author(aid),
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE uuser (
    uid INT PRIMARY KEY,
    uname VARCHAR(100)
);

CREATE TABLE comment (
  cid INT PRIMARY KEY,
  content VARCHAR(1000),
  post_id INT NOT NULL,
  FOREIGN KEY(post_id) REFERENCES post(pid),
  commented_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  user_id INT NOT NULL,
  FOREIGN KEY(user_id) REFERENCES uuser(uid)
);

INSERT INTO author(aid, aname) VALUES (1, 'James Bond'), (2, 'Bob Smith');

INSERT INTO post(pid, pname, author_id, created_on) VALUES
                                             (1, 'post1 abc',1, '2021-05-09 14:51:52.781000'),
                                             (2, 'post2 def',2, '2021-05-08 14:51:52.781000'),
                                             (3, 'post3 ghi',1, '2021-05-07 14:51:52.781000');

INSERT INTO uuser(uid, uname) VALUES (701, 'Richart Connors'),
                                     (702, 'Padraig Shropshire'),
                                     (703, 'Flore Curnick');

INSERT INTO comment (cid, content, post_id, commented_on, user_id) VALUES
(1, 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 1, '2021-06-29 14:51:53.358503', 701),
(2, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 2, '2021-06-28 14:51:53.358503', 702),
(3, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 1, '2021-06-27 14:51:53.358503', 703),
(4, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 2, '2021-06-26 14:51:53.358503', 701),
(5, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 1, '2021-06-25 14:51:53.358503', 702),
(6, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 2, '2021-06-24 14:51:53.358503', 703),
(7, 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 1, '2021-06-23 14:51:53.358503', 701),
(8, 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 2, '2021-06-22 14:51:53.358503', 702),
(9, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 1, '2021-06-21 14:51:53.358503', 703),
(10, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae, Duis faucibus accumsan odio. Curabitur convallis.', 2, '2021-06-20 14:51:53.358503', 701),
(11, 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 1, '2021-06-19 14:51:53.358503', 702),
(12, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 2, '2021-06-18 14:51:53.358503', 703),
(13, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 1, '2021-06-17 14:51:53.358503', 701),
(14, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 2, '2021-06-16 14:51:53.358503', 702),
(15, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1, '2021-06-15 14:51:53.358503', 703),
(16, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 2, '2021-06-14 14:51:53.358503', 701),
(17, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae, Duis faucibus accumsan odio. Curabitur convallis.', 1, '2021-06-13 14:51:53.358503', 702),
(18, 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 2, '2021-06-12 14:51:53.358503', 703),
(19, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1, '2021-06-11 14:51:53.358503', 701),
(20, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 2, '2021-06-10 14:51:53.358503', 702),
(21, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 1, '2021-06-09 14:51:53.358503', 703),
(22, 'Fusce consequat. Nulla nisl. Nunc nisl.', 2, '2021-06-08 14:51:53.358503', 701),
(23, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 1, '2021-06-07 14:51:53.358503', 702),
(24, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 2, '2021-06-06 14:51:53.358503', 703),
(25, 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 1, '2021-06-05 14:51:53.358503', 701),
(26, 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 2, '2021-06-04 14:51:53.358503', 702),
(27, 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 1, '2021-06-03 14:51:53.358503', 703),
(28, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2, '2021-06-02 14:51:53.358503', 701),
(29, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 1, '2021-06-01 14:51:53.358503', 702),
(30, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 2, '2021-05-31 14:51:53.358503', 703),
(31, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 1, '2021-05-30 14:51:53.358503', 701),
(32, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae, Mauris viverra diam vitae quam. Suspendisse potenti.', 2, '2021-05-29 14:51:53.358503', 702),
(33, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 1, '2021-05-28 14:51:53.358503', 703),
(34, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 2, '2021-05-27 14:51:53.358503', 701),
(35, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 3, '2021-05-26 14:51:53.358503', 702),
(36, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 2, '2021-05-25 14:51:53.358503', 703),
(37, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 3, '2021-05-24 14:51:53.358503', 701),
(38, 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 3, '2021-05-23 14:51:53.358503', 702),
(39, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, '2021-05-22 14:51:53.358503', 703),
(40, 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 3, '2021-05-21 14:51:53.358503', 701),
(41, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 3, '2021-05-20 14:51:53.358503', 702),
(42, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 3, '2021-05-19 14:51:53.358503', 703),
(43, 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 3, '2021-05-18 14:51:53.358503', 701),
(44, 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 3, '2021-05-17 14:51:53.358503', 702),
(45, 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 3, '2021-05-16 14:51:53.358503', 703),
(46, 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 3, '2021-05-15 14:51:53.358503', 701),
(47, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 3, '2021-05-14 14:51:53.358503', 702),
(48, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 3, '2021-05-13 14:51:53.358503', 703),
(49, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 3, '2021-05-12 14:51:53.358503', 701),
(50, 'In congue. Etiam justo. Etiam pretium iaculis justo.', 3, '2021-05-11 14:51:53.358503', 702);
