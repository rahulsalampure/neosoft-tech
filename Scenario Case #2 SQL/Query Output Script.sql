/* Title: Query output script  

Language used: PostgreSQL

Result Query: 
*/

SELECT t.r as row_no,
       t.aid as author_id ,
       t.aname as author_name ,
       t.pid as post_id,
       t.pname as post_name,
       t.created_on,
       t.content as comments,
       t.uname as username,
       t.commented_on
FROM (SELECT *, ROW_NUMBER () OVER (
        PARTITION BY post.pid
        ORDER BY comment.commented_on DESC) AS r
      FROM post
        INNER JOIN author ON post.author_id = author.aid
        INNER JOIN comment ON post.pid = comment.post_id
        INNER JOIN uuser on uuser.uid = comment.user_id
    WHERE author.aname='James Bond') AS t WHERE t.r <= 10;