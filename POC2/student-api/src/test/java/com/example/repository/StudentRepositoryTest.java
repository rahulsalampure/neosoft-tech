package com.example.repository;

import com.example.model.Photo;
import com.example.model.Project;
import com.example.model.Student;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @BeforeAll
    void setUp() {
        List<Student> users = Arrays.asList(
                new Student(901L, "FirstName1", "LastName1", "1111122222", "abc@xyz.com",
                        "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                        Arrays.asList(new Project(101L,101L, "Project1", "1 Month",new Student()),
                                new Project(102L,102L, "Project2", "2 Month", new Student())),
                        new Photo("Photo1","image/jpeg","string1".getBytes())),

                new Student(902L, "FirstName2", "LastName3", "1111133333", "def@xyz.com",
                        "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                        Arrays.asList(new Project(103L,101L, "Project1", "1 Month",new Student()),
                                new Project(104L,102L, "Project2", "2 Month", new Student())),
                        new Photo("Photo1","image/jpeg","string1".getBytes())),

                new Student(903L, "FirstName1", "LastName1", "1111144444", "ghi@xyz.com",
                        "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                        Arrays.asList(new Project(105L,101L, "Project1", "1 Month",new Student()),
                                new Project(2L,106L, "Project2", "2 Month", new Student())),
                        new Photo("Photo1","image/jpeg","string1".getBytes())));

        studentRepository.saveAll(users);
    }

    @AfterAll
    void tearDown() {
        studentRepository.deleteAll();
    }

    @Test
    public void findAllStudents() {
        List<Student> all = studentRepository.findAll();
        assertEquals(3, all.size());
    }

    @Test
    public void findStudentByID() {
        Student student = studentRepository.findById(901L).get();
        assertEquals(student.getId(), 901L);
    }

    @Test
    void findByStudentEmail() {
        Student student = studentRepository.findByEmail("abc@xyz.com");
        assertEquals(student.getEmail(), "abc@xyz.com");
    }

    @Test
    public void testCreateReadDelete() {
        Student student = new Student(501L, "FirstName1", "LastName1", "1111122222", "abc@xyz.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(301L,101L, "Project1", "1 Month",new Student()),
                        new Project(302L,102L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));

        studentRepository.save(student);
        Assertions.assertTrue(studentRepository.findAll().get(1).getFirstName().equals("FirstName1"));

        studentRepository.deleteAll();
        Assertions.assertTrue(studentRepository.findAll().isEmpty());
    }
}