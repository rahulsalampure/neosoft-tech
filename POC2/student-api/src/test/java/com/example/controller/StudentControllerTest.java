package com.example.controller;

import com.example.dto.StudentRequestDTO;
import com.example.model.Photo;
import com.example.model.Project;
import com.example.model.Student;
import com.example.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Arrays.asList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = StudentController.class)
class StudentControllerTest {

    @MockBean
    private StudentService studentService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    Student student1;

    @BeforeEach
    void setUp() {
        student1 = new Student(771L, "FirstName11", "LastName11", "1234540404", "pqrst@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                asList(new Project(1867L,101L, "Project1", "1 Month",new Student()),
                        new Project(1878L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));
    }

    @Test
    @DisplayName("Should List All Students When making GET request to endpoint - /students/student")
    public void shouldGetAllStudent() throws Exception {
        Student student2 = new Student(731L, "FirstName21", "LastName21", "1234511104", "mqrst@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                asList(new Project(1777L,101L, "Project1", "1 Month",new Student()),
                        new Project(1878L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));

        Mockito.when(studentService.findAllStudent()).thenReturn(asList(student1, student2));

        mockMvc.perform(get("/students/student"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(771)))
                .andExpect(jsonPath("$[0].firstName", Matchers.is("FirstName11")))
                .andExpect(jsonPath("$[1].firstName", Matchers.is("FirstName21")))
                .andExpect(jsonPath("$[1].id", Matchers.is(731)));
    }

    @Test
    @DisplayName("Should get Student By ID When making GET request to endpoint - /students/student/{id}")
    public void shouldGetStudentByID() throws Exception {
        Student student3 = new Student(772L, "FirstName12", "LastName12", "1114540404", "pqrstu@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                asList(new Project(1869L,101L, "Project1", "1 Month",new Student()),
                        new Project(1879L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));

        Mockito.when(studentService.findStudentById(771L)).thenReturn(student3);

        mockMvc.perform(get("/students/student/772"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()", Matchers.is(1)))
                .andExpect(jsonPath("$.id", Matchers.is(772)))
                .andExpect(jsonPath("$.firstName", Matchers.is("FirstName12")));
    }

    @Test
    @DisplayName("Should Save Student When making POST request to endpoint - /students/student")
    void shouldCreateStudent() throws Exception {
        StudentRequestDTO studentRequestDTO = new StudentRequestDTO(771L, "FirstName11", "LastName11", "1234540404", "pqrst@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                asList(new Project(1867L,101L, "Project1", "1 Month",new Student()),
                        new Project(1878L,106L, "Project2", "2 Month", new Student())));

        MultipartFile file = new MockMultipartFile(      "file", "hello.jpeg", MediaType.IMAGE_JPEG_VALUE,"Hello, World!".getBytes());

        Mockito.when(studentService.saveStudent(studentRequestDTO,file)).thenReturn(student1);

        mockMvc.perform(post("/students/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(studentRequestDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.size()", Matchers.is(1)))
                .andExpect(jsonPath("$.id", Matchers.is(413)))
                .andExpect(jsonPath("$.lastName", Matchers.is("LastName74")));
    }

    @Test
    @DisplayName("Should get Photo By ID When making GET request to endpoint - /student/photo/{id}")
    public void shouldGetPhotoByID() throws Exception {
        Photo photo =  new Photo("Photo1","image/jpeg","string1".getBytes());

        Mockito.when(studentService.findStudentById(771L).getPhoto()).thenReturn(photo);

        mockMvc.perform(get("/student/photo/771"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE));
    }
}