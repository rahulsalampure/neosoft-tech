package com.example.service;

import com.example.dto.StudentRequestDTO;
import com.example.model.Photo;
import com.example.model.Project;
import com.example.model.Student;
import com.example.repository.StudentRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentService studentService;

    @Captor
    private ArgumentCaptor<Student> postArgumentCaptor;

    @Test
    @DisplayName("Should Save Students")
    public void shouldSaveStudents() {
        StudentRequestDTO studentRequestDTO = new StudentRequestDTO(713L, "FirstName7", "LastName7", "5555544444", "ehi@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(155L,101L, "Project1", "1 Month",new Student()),
                        new Project(185L,106L, "Project2", "2 Month", new Student())));

        MultipartFile file = new MockMultipartFile(      "file", "hello.jpeg", MediaType.IMAGE_JPEG_VALUE,"Hello, World!".getBytes());
        studentService.saveStudent(studentRequestDTO,file);

        Mockito.verify(studentRepository, times(1)).save(postArgumentCaptor.capture());

        Assertions.assertThat(postArgumentCaptor.getValue().getFirstName()).isEqualTo("FirstName7");
        Assertions.assertThat(postArgumentCaptor.getValue().getEmail()).isEqualTo("ehi@abc.com");
    }


    @Test
    @DisplayName("Should Get All Students")
    public void shouldGetAllStudents() {
        List<Student> students = new ArrayList();
        students.add(new Student(801L, "FirstName1", "LastName1", "1010122222", "abc@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(181L,101L, "Project1", "1 Month",new Student()),
                        new Project(182L,102L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes())));

        students.add(new Student(802L, "FirstName2", "LastName2", "1010133333", "def@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(183L,101L, "Project1", "1 Month",new Student()),
                        new Project(184L,102L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes())));

        students.add(new Student(803L, "FirstName3", "LastName3", "1010144444", "ghi@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(185L,101L, "Project1", "1 Month",new Student()),
                        new Project(185L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes())));


        given(studentRepository.findAll()).willReturn(students);

        List<Student> expected = studentService.findAllStudent();

        assertEquals(expected, students);
    }

    @Test
    @DisplayName("Should Get Student By ID")
    public void shouldStudentById() {
        Student student = new Student(893L, "FirstName4", "LastName4", "1010140404", "pqr@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(186L,101L, "Project1", "1 Month",new Student()),
                        new Project(187L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));

        studentRepository.save(student);

        given(studentRepository.findById(893L).get()).willReturn(student);

        Student expected = studentService.findStudentById(893L);

        assertEquals(expected, student);
    }

    @Test
    @DisplayName("Should Get Student By ID")
    public void shouldLoadByUsernameIsEmail() {
        Student student = new Student(895L, "FirstName5", "LastName5", "1010120202", "rst@abc.com",
                "$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu",
                Arrays.asList(new Project(119L,101L, "Project1", "1 Month",new Student()),
                        new Project(120L,106L, "Project2", "2 Month", new Student())),
                new Photo("Photo1","image/jpeg","string1".getBytes()));

        UserDetails userDetails = new User(student.getEmail(), student.getPassword(),
                                           Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
        studentRepository.save(student);

        given(studentRepository.findByEmail("rst@abc.com")).willReturn(student);
        UserDetails expected = studentService.loadUserByUsername("rst@abc.com");

        assertEquals(expected, userDetails);
    }
}