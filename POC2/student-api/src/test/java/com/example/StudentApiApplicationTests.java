package com.example;

import com.example.controller.StudentController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class StudentApiApplicationTests {

    @Autowired
    StudentController studentController;

    @Test
    void contextLoads() {
        Assertions.assertTrue(!studentController.equals(null));
    }

}
