package com.example.dto;

import com.example.model.Project;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentResponseDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String mobileNo;

    private String email;

    private List<Project> projects= new ArrayList<>();

    private PhotoResponseDTO photoResponseDTO;
}
