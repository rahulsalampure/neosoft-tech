package com.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="student",uniqueConstraints = {@UniqueConstraint (columnNames = "student_id"),
                                           @UniqueConstraint (columnNames = "email")})
public class Student {

    @Id
    @Column(name = "student_id",unique = true,nullable = false,updatable = false)
    private Long id;

    @Column(name = "firstname",length = 50)
    private String firstName;

    @Column(name = "lastname",length = 50)
    private String lastName;

    @Pattern(regexp="^((?!(0))[0-9]{10})$",message="mobileNo must be of 10 digit")
    @Column(name="mobile_no",unique = true)
    private String mobileNo;

    @NotEmpty(message = "Email is required")
    @Email(message = "Email should be valid")
    @Column(name="email",unique = true)
    private String email;

    @JsonIgnore
    private String password;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "student_student_id")
    @OrderColumn(name = "type")
    private List<Project> projects= new ArrayList<>();

    private Photo photo;
}