package com.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seq")
    @SequenceGenerator(name = "seq",sequenceName = "project_id_seq")
    private Long id;

    @Column(name = "project_pid",nullable = false)
    private Long projectID;

    @Column(name = "project_name",length = 200)
    private String projectName;

    @Column(name = "duration",nullable = false)
    private String duration;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

}