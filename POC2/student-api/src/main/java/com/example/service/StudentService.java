package com.example.service;

import com.example.dto.StudentRequestDTO;
import com.example.exception.FileStorageException;
import com.example.model.Photo;
import com.example.model.Student;
import com.example.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service(value = "studentService")
@AllArgsConstructor
public class StudentService implements UserDetailsService {

    private static final Logger logger = LogManager.getLogger(StudentService.class.getName());

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public List<Student> findAllStudent(){
        return studentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Student findStudentById(final Long id){
        return studentRepository.existsById(id) ? studentRepository.findById(id).get() : new Student();
    }

    @Transactional
    public Student saveStudent(final StudentRequestDTO studentRequestDTO, MultipartFile file){
        try {
            studentRequestDTO.setPassword(passwordEncoder.encode(studentRequestDTO.getPassword()));
            Student student = copy(studentRequestDTO, file);
            return !studentRepository.existsById(student.getId()) ? studentRepository.save(student) : new Student();
        } catch (Exception exception) {
            logger.error("Error Occurred!!!: {}", exception);
        }
        return new Student();
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Student student = studentRepository.findByEmail(email);
        if(student == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new User(student.getEmail(), student.getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    public static Student copy(final StudentRequestDTO studentRequestDTO, MultipartFile file) {
        Photo photo = new Photo();
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            photo.setPhotoName(fileName);
            photo.setPhotoType(file.getContentType());
            photo.setData(file.getBytes());
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
        final Student student = new Student();
        student.setId(studentRequestDTO.getId());
        student.setFirstName(studentRequestDTO.getFirstName());
        student.setLastName(studentRequestDTO.getLastName());
        student.setMobileNo(studentRequestDTO.getMobileNo());
        student.setEmail(studentRequestDTO.getEmail());
        student.setPassword(studentRequestDTO.getPassword());
        student.setProjects(studentRequestDTO.getProjects());
        student.setPhoto(photo);

        return student;
    }
}
