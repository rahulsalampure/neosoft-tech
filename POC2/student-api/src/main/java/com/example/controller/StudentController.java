package com.example.controller;

import com.example.dto.PhotoResponseDTO;
import com.example.dto.StudentRequestDTO;
import com.example.dto.StudentResponseDTO;
import com.example.model.Photo;
import com.example.model.Student;
import com.example.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/students")
public class StudentController {

    private static final Logger logger = LogManager.getLogger(StudentController.class.getName());

    @Autowired
    private StudentService studentService;

    @GetMapping("/student")
    public ResponseEntity<List<StudentResponseDTO>> getStudent(){
        return reduce(studentService.findAllStudent());
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<StudentResponseDTO> getStudentById(@PathVariable("id") Long id){
        Student student = studentService.findStudentById(id);
        if(student.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(transform(student), HttpStatus.OK);
    }

    @PostMapping("/student")
    public ResponseEntity<StudentResponseDTO> saveStudent(@RequestParam("file") MultipartFile file,
                                                          @RequestParam("jsonString")String jsonString){
        StudentRequestDTO studentRequestDTO = null;
        try {
            studentRequestDTO = new ObjectMapper().readValue(jsonString, StudentRequestDTO.class);
        } catch (Exception e) {
            logger.error("Not Converting String to JSON!!!: {}",e);
        }
        return new ResponseEntity<>(transform(studentService.saveStudent(studentRequestDTO, file)), HttpStatus.CREATED);
    }

    @GetMapping("/student/photo/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long fileId) {
        Student student = studentService.findStudentById(fileId);
        Photo photo = student.getPhoto();
        if(student.getId() == null || photo.getPhotoName() == null || photo.getPhotoType() == null || photo.getData() == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(photo.getPhotoType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + photo.getPhotoName() + "\"")
                .body(new ByteArrayResource(photo.getData()));
    }

    public static ResponseEntity reduce(List<Student> students) {
        List<StudentResponseDTO> userResponseDTO = students.stream().map(StudentController::transform).collect(toList());
        return userResponseDTO.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND):
                new ResponseEntity<>(userResponseDTO, HttpStatus.OK);
    }

    public static StudentResponseDTO transform(final Student student) {

        final StudentResponseDTO studentResponseDTO = new StudentResponseDTO();
        studentResponseDTO.setId(student.getId());
        studentResponseDTO.setFirstName(student.getFirstName());
        studentResponseDTO.setLastName(student.getLastName());
        studentResponseDTO.setMobileNo(student.getMobileNo());
        studentResponseDTO.setEmail(student.getEmail());
        studentResponseDTO.setProjects(student.getProjects());
        studentResponseDTO.setPhotoResponseDTO(photoToDTO(student.getPhoto(),student.getId()));

        return studentResponseDTO;
    }

    public static PhotoResponseDTO photoToDTO(Photo photo,Long photoId) {
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/student/photo/")
                .path(photoId.toString())
                .toUriString();

        return new PhotoResponseDTO(photo.getPhotoName(), fileDownloadUri,
                photo.getPhotoType(), bytesToString(photo.getData().length));
    }

    public static String bytesToString(double bytes) {
        int one = 1024,
            kb = (int) Math.ceil(bytes / one),
            mb = (int) Math.ceil(kb / one),
            gb = (int) Math.ceil(mb / one);

        return (gb > 0) ? gb + " GB" : (mb > 0) ? mb + " MB" : kb + " KB";
    }
}
